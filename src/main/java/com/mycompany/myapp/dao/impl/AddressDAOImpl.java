/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dao.impl;


import com.mycompany.myapp.dao.AddressDAO;
import com.mycompany.myapp.dao.impl.exceptions.IllegalOrphanException;
import com.mycompany.myapp.dao.impl.exceptions.NonexistentEntityException;
import com.mycompany.myapp.dao.impl.exceptions.PreexistingEntityException;
import com.mycompany.myapp.entity.Address;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.mycompany.myapp.entity.SalesOrderHeader;
import java.util.ArrayList;
import java.util.List;
import com.mycompany.myapp.entity.CustomerAddress;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author nleit_000
 */
public class AddressDAOImpl implements AddressDAO, Serializable {

    public AddressDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Address address) throws PreexistingEntityException, Exception {
        if (address.getSalesOrderHeaderList() == null) {
            address.setSalesOrderHeaderList(new ArrayList<SalesOrderHeader>());
        }
        if (address.getSalesOrderHeaderList1() == null) {
            address.setSalesOrderHeaderList1(new ArrayList<SalesOrderHeader>());
        }
        if (address.getCustomerAddressList() == null) {
            address.setCustomerAddressList(new ArrayList<CustomerAddress>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<SalesOrderHeader> attachedSalesOrderHeaderList = new ArrayList<SalesOrderHeader>();
            for (SalesOrderHeader salesOrderHeaderListSalesOrderHeaderToAttach : address.getSalesOrderHeaderList()) {
                salesOrderHeaderListSalesOrderHeaderToAttach = em.getReference(salesOrderHeaderListSalesOrderHeaderToAttach.getClass(), salesOrderHeaderListSalesOrderHeaderToAttach.getSalesOrderID());
                attachedSalesOrderHeaderList.add(salesOrderHeaderListSalesOrderHeaderToAttach);
            }
            address.setSalesOrderHeaderList(attachedSalesOrderHeaderList);
            List<SalesOrderHeader> attachedSalesOrderHeaderList1 = new ArrayList<SalesOrderHeader>();
            for (SalesOrderHeader salesOrderHeaderList1SalesOrderHeaderToAttach : address.getSalesOrderHeaderList1()) {
                salesOrderHeaderList1SalesOrderHeaderToAttach = em.getReference(salesOrderHeaderList1SalesOrderHeaderToAttach.getClass(), salesOrderHeaderList1SalesOrderHeaderToAttach.getSalesOrderID());
                attachedSalesOrderHeaderList1.add(salesOrderHeaderList1SalesOrderHeaderToAttach);
            }
            address.setSalesOrderHeaderList1(attachedSalesOrderHeaderList1);
            List<CustomerAddress> attachedCustomerAddressList = new ArrayList<CustomerAddress>();
            for (CustomerAddress customerAddressListCustomerAddressToAttach : address.getCustomerAddressList()) {
                customerAddressListCustomerAddressToAttach = em.getReference(customerAddressListCustomerAddressToAttach.getClass(), customerAddressListCustomerAddressToAttach.getCustomerAddressPK());
                attachedCustomerAddressList.add(customerAddressListCustomerAddressToAttach);
            }
            address.setCustomerAddressList(attachedCustomerAddressList);
            em.persist(address);
            for (SalesOrderHeader salesOrderHeaderListSalesOrderHeader : address.getSalesOrderHeaderList()) {
                Address oldShipToAddressIDOfSalesOrderHeaderListSalesOrderHeader = salesOrderHeaderListSalesOrderHeader.getShipToAddressID();
                salesOrderHeaderListSalesOrderHeader.setShipToAddressID(address);
                salesOrderHeaderListSalesOrderHeader = em.merge(salesOrderHeaderListSalesOrderHeader);
                if (oldShipToAddressIDOfSalesOrderHeaderListSalesOrderHeader != null) {
                    oldShipToAddressIDOfSalesOrderHeaderListSalesOrderHeader.getSalesOrderHeaderList().remove(salesOrderHeaderListSalesOrderHeader);
                    oldShipToAddressIDOfSalesOrderHeaderListSalesOrderHeader = em.merge(oldShipToAddressIDOfSalesOrderHeaderListSalesOrderHeader);
                }
            }
            for (SalesOrderHeader salesOrderHeaderList1SalesOrderHeader : address.getSalesOrderHeaderList1()) {
                Address oldBillToAddressIDOfSalesOrderHeaderList1SalesOrderHeader = salesOrderHeaderList1SalesOrderHeader.getBillToAddressID();
                salesOrderHeaderList1SalesOrderHeader.setBillToAddressID(address);
                salesOrderHeaderList1SalesOrderHeader = em.merge(salesOrderHeaderList1SalesOrderHeader);
                if (oldBillToAddressIDOfSalesOrderHeaderList1SalesOrderHeader != null) {
                    oldBillToAddressIDOfSalesOrderHeaderList1SalesOrderHeader.getSalesOrderHeaderList1().remove(salesOrderHeaderList1SalesOrderHeader);
                    oldBillToAddressIDOfSalesOrderHeaderList1SalesOrderHeader = em.merge(oldBillToAddressIDOfSalesOrderHeaderList1SalesOrderHeader);
                }
            }
            for (CustomerAddress customerAddressListCustomerAddress : address.getCustomerAddressList()) {
                Address oldAddressOfCustomerAddressListCustomerAddress = customerAddressListCustomerAddress.getAddress();
                customerAddressListCustomerAddress.setAddress(address);
                customerAddressListCustomerAddress = em.merge(customerAddressListCustomerAddress);
                if (oldAddressOfCustomerAddressListCustomerAddress != null) {
                    oldAddressOfCustomerAddressListCustomerAddress.getCustomerAddressList().remove(customerAddressListCustomerAddress);
                    oldAddressOfCustomerAddressListCustomerAddress = em.merge(oldAddressOfCustomerAddressListCustomerAddress);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAddress(address.getAddressID()) != null) {
                throw new PreexistingEntityException("Address " + address + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Address address) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Address persistentAddress = em.find(Address.class, address.getAddressID());
            List<SalesOrderHeader> salesOrderHeaderListOld = persistentAddress.getSalesOrderHeaderList();
            List<SalesOrderHeader> salesOrderHeaderListNew = address.getSalesOrderHeaderList();
            List<SalesOrderHeader> salesOrderHeaderList1Old = persistentAddress.getSalesOrderHeaderList1();
            List<SalesOrderHeader> salesOrderHeaderList1New = address.getSalesOrderHeaderList1();
            List<CustomerAddress> customerAddressListOld = persistentAddress.getCustomerAddressList();
            List<CustomerAddress> customerAddressListNew = address.getCustomerAddressList();
            List<String> illegalOrphanMessages = null;
            for (CustomerAddress customerAddressListOldCustomerAddress : customerAddressListOld) {
                if (!customerAddressListNew.contains(customerAddressListOldCustomerAddress)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain CustomerAddress " + customerAddressListOldCustomerAddress + " since its address field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<SalesOrderHeader> attachedSalesOrderHeaderListNew = new ArrayList<SalesOrderHeader>();
            for (SalesOrderHeader salesOrderHeaderListNewSalesOrderHeaderToAttach : salesOrderHeaderListNew) {
                salesOrderHeaderListNewSalesOrderHeaderToAttach = em.getReference(salesOrderHeaderListNewSalesOrderHeaderToAttach.getClass(), salesOrderHeaderListNewSalesOrderHeaderToAttach.getSalesOrderID());
                attachedSalesOrderHeaderListNew.add(salesOrderHeaderListNewSalesOrderHeaderToAttach);
            }
            salesOrderHeaderListNew = attachedSalesOrderHeaderListNew;
            address.setSalesOrderHeaderList(salesOrderHeaderListNew);
            List<SalesOrderHeader> attachedSalesOrderHeaderList1New = new ArrayList<SalesOrderHeader>();
            for (SalesOrderHeader salesOrderHeaderList1NewSalesOrderHeaderToAttach : salesOrderHeaderList1New) {
                salesOrderHeaderList1NewSalesOrderHeaderToAttach = em.getReference(salesOrderHeaderList1NewSalesOrderHeaderToAttach.getClass(), salesOrderHeaderList1NewSalesOrderHeaderToAttach.getSalesOrderID());
                attachedSalesOrderHeaderList1New.add(salesOrderHeaderList1NewSalesOrderHeaderToAttach);
            }
            salesOrderHeaderList1New = attachedSalesOrderHeaderList1New;
            address.setSalesOrderHeaderList1(salesOrderHeaderList1New);
            List<CustomerAddress> attachedCustomerAddressListNew = new ArrayList<CustomerAddress>();
            for (CustomerAddress customerAddressListNewCustomerAddressToAttach : customerAddressListNew) {
                customerAddressListNewCustomerAddressToAttach = em.getReference(customerAddressListNewCustomerAddressToAttach.getClass(), customerAddressListNewCustomerAddressToAttach.getCustomerAddressPK());
                attachedCustomerAddressListNew.add(customerAddressListNewCustomerAddressToAttach);
            }
            customerAddressListNew = attachedCustomerAddressListNew;
            address.setCustomerAddressList(customerAddressListNew);
            address = em.merge(address);
            for (SalesOrderHeader salesOrderHeaderListOldSalesOrderHeader : salesOrderHeaderListOld) {
                if (!salesOrderHeaderListNew.contains(salesOrderHeaderListOldSalesOrderHeader)) {
                    salesOrderHeaderListOldSalesOrderHeader.setShipToAddressID(null);
                    salesOrderHeaderListOldSalesOrderHeader = em.merge(salesOrderHeaderListOldSalesOrderHeader);
                }
            }
            for (SalesOrderHeader salesOrderHeaderListNewSalesOrderHeader : salesOrderHeaderListNew) {
                if (!salesOrderHeaderListOld.contains(salesOrderHeaderListNewSalesOrderHeader)) {
                    Address oldShipToAddressIDOfSalesOrderHeaderListNewSalesOrderHeader = salesOrderHeaderListNewSalesOrderHeader.getShipToAddressID();
                    salesOrderHeaderListNewSalesOrderHeader.setShipToAddressID(address);
                    salesOrderHeaderListNewSalesOrderHeader = em.merge(salesOrderHeaderListNewSalesOrderHeader);
                    if (oldShipToAddressIDOfSalesOrderHeaderListNewSalesOrderHeader != null && !oldShipToAddressIDOfSalesOrderHeaderListNewSalesOrderHeader.equals(address)) {
                        oldShipToAddressIDOfSalesOrderHeaderListNewSalesOrderHeader.getSalesOrderHeaderList().remove(salesOrderHeaderListNewSalesOrderHeader);
                        oldShipToAddressIDOfSalesOrderHeaderListNewSalesOrderHeader = em.merge(oldShipToAddressIDOfSalesOrderHeaderListNewSalesOrderHeader);
                    }
                }
            }
            for (SalesOrderHeader salesOrderHeaderList1OldSalesOrderHeader : salesOrderHeaderList1Old) {
                if (!salesOrderHeaderList1New.contains(salesOrderHeaderList1OldSalesOrderHeader)) {
                    salesOrderHeaderList1OldSalesOrderHeader.setBillToAddressID(null);
                    salesOrderHeaderList1OldSalesOrderHeader = em.merge(salesOrderHeaderList1OldSalesOrderHeader);
                }
            }
            for (SalesOrderHeader salesOrderHeaderList1NewSalesOrderHeader : salesOrderHeaderList1New) {
                if (!salesOrderHeaderList1Old.contains(salesOrderHeaderList1NewSalesOrderHeader)) {
                    Address oldBillToAddressIDOfSalesOrderHeaderList1NewSalesOrderHeader = salesOrderHeaderList1NewSalesOrderHeader.getBillToAddressID();
                    salesOrderHeaderList1NewSalesOrderHeader.setBillToAddressID(address);
                    salesOrderHeaderList1NewSalesOrderHeader = em.merge(salesOrderHeaderList1NewSalesOrderHeader);
                    if (oldBillToAddressIDOfSalesOrderHeaderList1NewSalesOrderHeader != null && !oldBillToAddressIDOfSalesOrderHeaderList1NewSalesOrderHeader.equals(address)) {
                        oldBillToAddressIDOfSalesOrderHeaderList1NewSalesOrderHeader.getSalesOrderHeaderList1().remove(salesOrderHeaderList1NewSalesOrderHeader);
                        oldBillToAddressIDOfSalesOrderHeaderList1NewSalesOrderHeader = em.merge(oldBillToAddressIDOfSalesOrderHeaderList1NewSalesOrderHeader);
                    }
                }
            }
            for (CustomerAddress customerAddressListNewCustomerAddress : customerAddressListNew) {
                if (!customerAddressListOld.contains(customerAddressListNewCustomerAddress)) {
                    Address oldAddressOfCustomerAddressListNewCustomerAddress = customerAddressListNewCustomerAddress.getAddress();
                    customerAddressListNewCustomerAddress.setAddress(address);
                    customerAddressListNewCustomerAddress = em.merge(customerAddressListNewCustomerAddress);
                    if (oldAddressOfCustomerAddressListNewCustomerAddress != null && !oldAddressOfCustomerAddressListNewCustomerAddress.equals(address)) {
                        oldAddressOfCustomerAddressListNewCustomerAddress.getCustomerAddressList().remove(customerAddressListNewCustomerAddress);
                        oldAddressOfCustomerAddressListNewCustomerAddress = em.merge(oldAddressOfCustomerAddressListNewCustomerAddress);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = address.getAddressID();
                if (findAddress(id) == null) {
                    throw new NonexistentEntityException("The address with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Address address;
            try {
                address = em.getReference(Address.class, id);
                address.getAddressID();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The address with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<CustomerAddress> customerAddressListOrphanCheck = address.getCustomerAddressList();
            for (CustomerAddress customerAddressListOrphanCheckCustomerAddress : customerAddressListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Address (" + address + ") cannot be destroyed since the CustomerAddress " + customerAddressListOrphanCheckCustomerAddress + " in its customerAddressList field has a non-nullable address field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<SalesOrderHeader> salesOrderHeaderList = address.getSalesOrderHeaderList();
            for (SalesOrderHeader salesOrderHeaderListSalesOrderHeader : salesOrderHeaderList) {
                salesOrderHeaderListSalesOrderHeader.setShipToAddressID(null);
                salesOrderHeaderListSalesOrderHeader = em.merge(salesOrderHeaderListSalesOrderHeader);
            }
            List<SalesOrderHeader> salesOrderHeaderList1 = address.getSalesOrderHeaderList1();
            for (SalesOrderHeader salesOrderHeaderList1SalesOrderHeader : salesOrderHeaderList1) {
                salesOrderHeaderList1SalesOrderHeader.setBillToAddressID(null);
                salesOrderHeaderList1SalesOrderHeader = em.merge(salesOrderHeaderList1SalesOrderHeader);
            }
            em.remove(address);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Address> findAddressEntities() {
        return findAddressEntities(true, -1, -1);
    }

    public List<Address> findAddressEntities(int maxResults, int firstResult) {
        return findAddressEntities(false, maxResults, firstResult);
    }

    private List<Address> findAddressEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Address.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Address findAddress(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Address.class, id);
        } finally {
            em.close();
        }
    }

    public int getAddressCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Address> rt = cq.from(Address.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
