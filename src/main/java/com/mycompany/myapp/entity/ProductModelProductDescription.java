/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "ProductModelProductDescription", catalog = "AdventureWorksLT2012", schema = "SalesLT")
@NamedQueries({
    @NamedQuery(name = "ProductModelProductDescription.findAll", query = "SELECT p FROM ProductModelProductDescription p")
    , @NamedQuery(name = "ProductModelProductDescription.findByProductModelID", query = "SELECT p FROM ProductModelProductDescription p WHERE p.productModelProductDescriptionPK.productModelID = :productModelID")
    , @NamedQuery(name = "ProductModelProductDescription.findByProductDescriptionID", query = "SELECT p FROM ProductModelProductDescription p WHERE p.productModelProductDescriptionPK.productDescriptionID = :productDescriptionID")
    , @NamedQuery(name = "ProductModelProductDescription.findByCulture", query = "SELECT p FROM ProductModelProductDescription p WHERE p.productModelProductDescriptionPK.culture = :culture")
    , @NamedQuery(name = "ProductModelProductDescription.findByRowguid", query = "SELECT p FROM ProductModelProductDescription p WHERE p.rowguid = :rowguid")
    , @NamedQuery(name = "ProductModelProductDescription.findByModifiedDate", query = "SELECT p FROM ProductModelProductDescription p WHERE p.modifiedDate = :modifiedDate")})
public class ProductModelProductDescription implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProductModelProductDescriptionPK productModelProductDescriptionPK;
    @Basic(optional = false)
    @Column(name = "rowguid")
    private String rowguid;
    @Basic(optional = false)
    @Column(name = "ModifiedDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedDate;
    @JoinColumn(name = "ProductDescriptionID", referencedColumnName = "ProductDescriptionID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ProductDescription productDescription;
    @JoinColumn(name = "ProductModelID", referencedColumnName = "ProductModelID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ProductModel productModel;

    public ProductModelProductDescription() {
    }

    public ProductModelProductDescription(ProductModelProductDescriptionPK productModelProductDescriptionPK) {
        this.productModelProductDescriptionPK = productModelProductDescriptionPK;
    }

    public ProductModelProductDescription(ProductModelProductDescriptionPK productModelProductDescriptionPK, String rowguid, Date modifiedDate) {
        this.productModelProductDescriptionPK = productModelProductDescriptionPK;
        this.rowguid = rowguid;
        this.modifiedDate = modifiedDate;
    }

    public ProductModelProductDescription(int productModelID, int productDescriptionID, String culture) {
        this.productModelProductDescriptionPK = new ProductModelProductDescriptionPK(productModelID, productDescriptionID, culture);
    }

    public ProductModelProductDescriptionPK getProductModelProductDescriptionPK() {
        return productModelProductDescriptionPK;
    }

    public void setProductModelProductDescriptionPK(ProductModelProductDescriptionPK productModelProductDescriptionPK) {
        this.productModelProductDescriptionPK = productModelProductDescriptionPK;
    }

    public String getRowguid() {
        return rowguid;
    }

    public void setRowguid(String rowguid) {
        this.rowguid = rowguid;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public ProductDescription getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(ProductDescription productDescription) {
        this.productDescription = productDescription;
    }

    public ProductModel getProductModel() {
        return productModel;
    }

    public void setProductModel(ProductModel productModel) {
        this.productModel = productModel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productModelProductDescriptionPK != null ? productModelProductDescriptionPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductModelProductDescription)) {
            return false;
        }
        ProductModelProductDescription other = (ProductModelProductDescription) object;
        if ((this.productModelProductDescriptionPK == null && other.productModelProductDescriptionPK != null) || (this.productModelProductDescriptionPK != null && !this.productModelProductDescriptionPK.equals(other.productModelProductDescriptionPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.entity.ProductModelProductDescription[ productModelProductDescriptionPK=" + productModelProductDescriptionPK + " ]";
    }
    
}
