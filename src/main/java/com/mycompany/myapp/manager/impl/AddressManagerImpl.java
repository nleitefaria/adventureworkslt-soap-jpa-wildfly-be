package com.mycompany.myapp.manager.impl;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dao.AddressDAO;
import com.mycompany.myapp.dao.impl.AddressDAOImpl;
import com.mycompany.myapp.manager.AddressManager;

public class AddressManagerImpl implements AddressManager {
	
	public  AddressManagerImpl()
	{
	}

	public Integer getAddressCount()
    { 
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPASQLServerPU");   
		AddressDAO addressDao = new AddressDAOImpl(emf);      
        return addressDao.getAddressCount();
    }

}
