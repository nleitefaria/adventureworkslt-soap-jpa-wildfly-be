package com.mycompany.myapp.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.mycompany.myapp.manager.AddressManager;
import com.mycompany.myapp.manager.impl.AddressManagerImpl;

@WebService
public class AddressService 
{
	private AddressManager addressManager;
	
    public AddressService()
    {
    	addressManager = new AddressManagerImpl();
    }
    
    @WebMethod
    public Integer getAddressCount()
    {  	
        return addressManager.getAddressCount();
    }

}
