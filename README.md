SOAP Webservices over the sample AdventureWorksLT database using:

- SOAP
- Wildfly
- JavaEE
- JPA
- SQLServer

![picture](http://dba.fyicenter.com/faq/sql_server/AdventureWorksLT.jpg)